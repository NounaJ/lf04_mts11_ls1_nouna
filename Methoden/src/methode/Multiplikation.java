package methode;

public class Multiplikation
{
	public static void main(String[]args)
	{
		double x = 2.36D;
		double y = 7.87D;
		double ergebnis = x * y;
		
		verarbeitung(x, y, ergebnis);
		
		ausgabe(x, y, ergebnis);
	}
	
	public static double verarbeitung(double x, double y, double ergebnis)
	{
		ergebnis = x * y;
		return ergebnis;
	}
	
	
	public static void ausgabe(double x, double y, double ergebnis)
	{
		System.out.printf("Das Ergebnis von %.2f und %.2f ist %.4f", x, y, ergebnis);
	}
}