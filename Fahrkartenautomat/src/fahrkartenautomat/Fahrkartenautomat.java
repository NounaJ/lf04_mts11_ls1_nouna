package fahrkartenautomat;
import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; // double; Zuweisungsoperator = ; Relationale Operation < ; Arithmetische Operation - ;
       double eingezahlterGesamtbetrag; // double; Zuweisungsoperator = , += ; Relationale Operation < 
       double eingeworfeneMuenze; // double; Zuweisungsoperator = , += ; 
       double rueckgabebetrag; // double; Zuweisungsoperator = , -= ; Arithmetischer Operationen  - ; Relationale Operation >=
       int anzahlTickets; // int; Arithmetische Operation * ; Zuweisungsoperator =
       double nochzuzahlen;
       
       anzahlTickets = tastatur.nextInt();
       zuZahlenderBetrag = tastatur.nextDouble() * (double)anzahlTickets;
       zuZahlenderBetrag = fahrkartenbestellungErfassen(zuZahlenderBetrag, tastatur);

       // Geldeinwurf
       // -----------
       nochzuzahlen = 0;
       fahrkartenBezahlen(zuZahlenderBetrag, nochzuzahlen, tastatur);
       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgeben();

       // Rueckgeldberechnung und -Ausgabe
       // -------------------------------
       eingezahlterGesamtbetrag = 0;
       rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       rueckgeldAusgeben(rueckgabebetrag);
    }
       
    public static double fahrkartenbestellungErfassen(double zuZahlenderBetrag, Scanner tastatur)
    {
    	tastatur = new Scanner(System.in);
    	System.out.print("Wie viele Tickets moechtest du:");
        System.out.print("Wie viel kostet ein Ticket: ");
        tastatur.close();
    	return zuZahlenderBetrag;
    }
    public static  double fahrkartenBezahlen(double zuZahlenderBetrag, double nochzuzahlen, Scanner tastatur)
    {
    	tastatur = new Scanner(System.in);
    	double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   nochzuzahlen = zuZahlenderBetrag - eingezahlterGesamtbetrag;
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n",nochzuzahlen);
     	   System.out.print("\nEingabe (mind. 5Ct, hoechstens 2 Euro): ");
     	   double eingeworfeneMuenze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMuenze;
           tastatur.close(); 
        }
        return nochzuzahlen;
    }
    public static void fahrkartenAusgeben()
    {
    	 System.out.println("\nFahrschein wird ausgegeben");
         for (int i = 0; i < 8; i++)
         {
            System.out.print("=");
            try {
  			Thread.sleep(250);
  		} catch (InterruptedException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
         }
         System.out.println("\n\n");
    }
    public static double rueckgeldAusgeben(double rueckgabebetrag)
    {
         if(rueckgabebetrag > 0.0)
         {
      	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO\n", rueckgabebetrag);
      	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

             while(rueckgabebetrag >= 2.00) // 2 EURO-M�nzen
             {
          	  System.out.println("2 EURO");
  	          rueckgabebetrag -= 2.0;
             }
             while(rueckgabebetrag >= 1.0) // 1 EURO-M�nzen
             {
          	  System.out.println("1 EURO");
  	          rueckgabebetrag -= 1.0;
             }
             while(rueckgabebetrag >= 0.50) // 50 CENT-M�nzen
             {
          	  System.out.println("50 CENT");
  	          rueckgabebetrag -= 0.5;
             }
             while(rueckgabebetrag >= 0.2) // 20 CENT-M�nzen
             {
          	  System.out.println("20 CENT");
   	          rueckgabebetrag -= 0.2;
             }
             while(rueckgabebetrag >= 0.1) // 10 CENT-M�nzen
             {
          	  System.out.println("10 CENT");
  	          rueckgabebetrag -= 0.1;
             }
             while(rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
             {
          	  System.out.println("5 CENT");
   	          rueckgabebetrag -= 0.05;
             }
         }
         return rueckgabebetrag;
    }
}

/*
 * In Zeile 14 erstelle ich den Int anzahlTickets und multipliziere meinen Input mit dem Input f�r den ZuzahlendenBetrag um den Endbetrag zu erhalten. Dabei caste ich anzahlTickets in ein double explizit, damit
 * ein double auch als Ergebnis kommt, da wir mit Geldbetr�gen arbeiten.
 */
