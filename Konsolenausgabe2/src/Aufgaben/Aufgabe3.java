package Aufgaben;

public class Aufgabe3 {
	public static void main (String[]args) { 
		
		
		System.out.printf("%-12s %s %10s","Fahrenheit","|","Celsius");
		System.out.printf("\n%s", "-------------------------");
		System.out.printf("\n%+-12d %s %10.2f", -20,"|", -28.8889);
		System.out.printf("\n%+-12d %s %10.2f",-10, "|", -23.3333);
		System.out.printf("\n%+-12d %s %10.2f",  0, "|", -17.7778);
		System.out.printf("\n%+-12d %s %10.2f", 10, "|",  -6.6667);
		System.out.printf("\n%+-12d %s %10.2f", 20, "|",  -1.1111);
	}
}
