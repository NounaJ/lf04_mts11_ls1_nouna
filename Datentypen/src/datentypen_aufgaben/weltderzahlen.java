/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 08.10.2021
  * @author << Jaouad Nouna >>
  */
package datentypen_aufgaben;

public class weltderzahlen
{
	
	public static void main (String[]args)
	{
		/*  *********************************************************
	    
        Zuerst werden die Variablen mit den Werten festgelegt!
   
		*********************************************************** */
		// Im Internet gefunden ?
	    // Die Anzahl der Planeten in unserem Sonnesystem 
		int anzahlPlaneten = 8;
		// Anzahl der Sterne in unserer Milchstra�e
		long anzahlSterne = 40000000000l;
		// Wie viele Einwohner hat Berlin?
		int bewohnerBerlin = 3645000;
	    // Wie alt bist du?  Wie viele Tage sind das?
		int alterTage = 7913;
		// Wie viel wiegt das schwerste Tier der Welt?
	    // Schreiben Sie das Gewicht in Kilogramm auf!
		int gewichtKilogramm = 15000;
		// Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
		int flaecheGrossteLand = 17098242;
		// Wie gro� ist das kleinste Land der Erde?
		float flaecheKleinsteLand = 0.44f; 
	    /*  *********************************************************
	    
	    Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
	    
	    *********************************************************** */
		System.out.println("Anzahl der Planeten in unserem Sonnensystem: " + anzahlPlaneten + " Planten");
		System.out.println("Anzahl der Sterne: " + anzahlSterne);
		System.out.println("Anzahl der Bewohner in Berlin: " + bewohnerBerlin);
		System.out.println("Dein Alter in Tagen: " + alterTage +" Tage");
		System.out.println("Das schwerste Tier der Welt: " + gewichtKilogramm +" Kilogramm");
		System.out.println("Flaeche des groessten Landes: " + flaecheGrossteLand + " Quadratkilometer");
		System.out.println("Flaeche des kleinsten Landes: " + flaecheKleinsteLand + " Quadratkilometer");
		System.out.println("******* Ende des Programms *******");
	}
}